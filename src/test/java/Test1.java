import com.beust.ah.A;
import org.apache.commons.compress.harmony.pack200.PackingUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import java.time.Duration;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
public class Test1 {
    WebDriver driver;
    WebDriverWait wait;
    JavascriptExecutor js;


    @BeforeMethod
    public void setUp() {
        driver = new ChromeDriver();
        wait = new WebDriverWait(driver, Duration.ofSeconds(10));
        js = (JavascriptExecutor) driver;
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
        driver.get("https://demoqa.com/");
        driver.manage().window().maximize();

    }
    @Test//1
    public void checking() {
        Assert.assertEquals(driver.getTitle(),"DEMOQA");
    }
   @Test//2
    public void getText ()  {
       List<WebElement> cards = driver.findElements(By.cssSelector(".category-cards>div"));
       WebElement card = cards.get(0);
       js.executeScript("arguments[0].scrollIntoView();", card);
       card.click();
       List<WebElement> items = driver.findElements(By.cssSelector("ul>#item-0"));
       WebElement item = items.get(0);
       item.click();
       WebElement useerName= driver.findElement(By.cssSelector("input[id=userName]"));
        useerName.sendKeys("Bakhish");
       WebElement userEmail= driver.findElement(By.cssSelector("input[id=userEmail]"));
       userEmail.sendKeys("jafarovbakhish@gmail.com");
       WebElement currentAdress= driver.findElement(By.cssSelector("[id=currentAddress]"));
       currentAdress.sendKeys("Baku,Azerbaijan");
       WebElement permanentAddress= driver.findElement(By.cssSelector("[id=permanentAddress]"));
       permanentAddress.sendKeys("Baku,Azerbaijan");
       WebElement submit= driver.findElement(By.cssSelector("[id=submit]"));
       js.executeScript("arguments[0].scrollIntoView();", submit);
       submit.click();
       WebElement name= driver.findElement(By.cssSelector("[id=name]"));
       Assert.assertEquals(name.getText(),"Name:Bakhish");
       WebElement email= driver.findElement(By.cssSelector("[id=email]"));
       Assert.assertEquals(email.getText(),"Email:jafarovbakhish@gmail.com");
       WebElement currentAdres = driver.findElement(By.cssSelector("p[id=\"currentAddress\"]"));
       Assert.assertEquals(currentAdres.getText(),"Current Address :Baku,Azerbaijan");
       WebElement permanentAddres = driver.findElement(By.cssSelector("p[id=\"permanentAddress\"]"));
       Assert.assertEquals(permanentAddres.getText(),"Permananet Address :Baku,Azerbaijan");
    }
    @Test//3
    public void Home() throws InterruptedException {
        List<WebElement> cards = driver.findElements(By.cssSelector(".category-cards>div"));
        WebElement card = cards.get(0);
        js.executeScript("arguments[0].scrollIntoView();", card);
        card.click();
        List<WebElement> checkBoxs = driver.findElements(By.cssSelector("ul>#item-1"));
        WebElement checkBox = checkBoxs.get(0);
        checkBox.click();
        WebElement homme= driver.findElement(By.cssSelector("#tree-node > ol > li > span > button > svg"));
        homme.click();
        Thread.sleep(3000);
        WebElement desktop= driver.findElement(By.cssSelector("#tree-node > ol > li > ol > li:nth-child(1) > span > label > span.rct-title"));
        desktop.click();
        Thread.sleep(3000);
        WebElement desktopTest= driver.findElement(By.cssSelector("#result > span:nth-child(2)"));
        WebElement notesTest= driver.findElement(By.cssSelector("#result > span:nth-child(3)"));
        WebElement commanddsTest= driver.findElement(By.cssSelector("#result > span:nth-child(4)"));
        Assert.assertEquals(desktopTest.getText(),"desktop");
        Assert.assertEquals(notesTest.getText(),"notes");
        Assert.assertEquals(commanddsTest.getText(),"commands");
    }
    @Test//4
    public void rightClickTest(){
        List<WebElement> cards = driver.findElements(By.cssSelector(".category-cards>div"));
        WebElement card = cards.get(0);
        js.executeScript("arguments[0].scrollIntoView();", card);
        card.click();
        List<WebElement> Buttons = driver.findElements(By.cssSelector("ul>#item-4"));
        WebElement Button = Buttons.get(0);
        Button.click();
        WebElement clickRightMe=driver.findElement(By.cssSelector("#rightClickBtn"));
        Actions actions = new Actions(driver);
        actions.contextClick(clickRightMe).perform();
        WebElement rightClickMessage =driver.findElement(By.cssSelector("#rightClickMessage"));
        Assert.assertEquals(rightClickMessage.getText(),"You have done a right click");
    }
    @Test//5
    public void uploadTest() {
        List<WebElement> cards = driver.findElements(By.cssSelector(".category-cards>div"));
        WebElement card = cards.get(0);
        js.executeScript("arguments[0].scrollIntoView();", card);
        card.click();
        List<WebElement> listItems = driver.findElements(By.cssSelector("ul>#item-7"));
        WebElement uploadAndDownload = listItems.get(0);
        js.executeScript("arguments[0].scrollIntoView();", uploadAndDownload);
        uploadAndDownload.click();
        WebElement fileInput = driver.findElement(By.cssSelector("#uploadFile"));
        String path = "C:\\Users\\User\\OneDrive\\Documents";
        fileInput.sendKeys(path);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#uploadedFilePath")));
        Assert.assertTrue(fileInput.isDisplayed());
    }
    @Test//6
    public void waitTest() {
        List<WebElement> cards = driver.findElements(By.cssSelector(".category-cards>div"));
        WebElement card = cards.get(0);
        js.executeScript("arguments[0].scrollIntoView();", card);
        card.click();
        List<WebElement> listItems = driver.findElements(By.cssSelector("ul>#item-8"));
        WebElement DynamicProperties = listItems.get(0);
        DynamicProperties.click();
        WebElement VisibleAfterButton = driver.findElement(By.cssSelector("#visibleAfter"));
        wait.until(ExpectedConditions.visibilityOf(VisibleAfterButton));
        Assert.assertEquals(VisibleAfterButton.getText(),"Visible After 5 Seconds");
    }
    @Test//7
    public void newTabTest() {
        List<WebElement> listOfCards = driver.findElements(By.cssSelector(".category-cards>div"));
        WebElement card = listOfCards.get(2);
        js.executeScript("arguments[0].scrollIntoView();", card);
        card.click();
        List<WebElement> listOfItems = driver.findElements(By.cssSelector("ul>#item-0"));
        WebElement BrowserWindows = listOfItems.get(2);
        BrowserWindows.click();
        WebElement newTab = driver.findElement(By.cssSelector("#tabButton"));
        newTab.click();
        String mainWindow = driver.getWindowHandle();
        Set<String> windowsId = driver.getWindowHandles();
        Iterator<String> iterator = windowsId.iterator();
        while (iterator.hasNext()) {
            String childWindow = iterator.next();
            if (!mainWindow.equalsIgnoreCase(childWindow)) {

                driver.switchTo().window(childWindow);
                WebElement samplePage = driver.findElement(By.cssSelector("#sampleHeading"));
                Assert.assertEquals(samplePage.getText(), "This is a sample page");


            }
        }
    }
    @Test//8
    public void alretInput(){
        List<WebElement> AlertsFramesWindowsOptions = driver.findElements(By.cssSelector(".category-cards>div"));
        WebElement card = AlertsFramesWindowsOptions.get(2);
        js.executeScript("arguments[0].scrollIntoView();", card);
        card.click();
        List<WebElement> Alerts = driver.findElements(By.cssSelector("ul>#item-1"));
        WebElement alert = Alerts.get(1);
        alert.click();
        WebElement clickMe= driver.findElement(By.cssSelector("#promtButton"));
        clickMe.click();
        driver.switchTo().alert().sendKeys("perfect");
        driver.switchTo().alert().accept();
        WebElement alretMessage= driver.findElement(By.cssSelector("#promptResult"));
        Assert.assertEquals(alretMessage.getText(),"You entered perfect");
    }
    @Test//9
    public void alret(){
        List<WebElement> AlertCards = driver.findElements(By.cssSelector(".category-cards>div"));
        WebElement cardAlertsFrameWindows = AlertCards.get(2);
        js.executeScript("arguments[0].scrollIntoView();", cardAlertsFrameWindows);
        cardAlertsFrameWindows.click();
        List<WebElement> listItems = driver.findElements(By.cssSelector(".menu-list>#item-1"));
        WebElement alerts = listItems.get(1);
        alerts.click();
        WebElement clickMe= driver.findElement(By.cssSelector("#confirmButton"));
        clickMe.click();
        driver.switchTo().alert().dismiss();
        WebElement dismissMessage=driver.findElement(By.cssSelector("#confirmResult"));
        Assert.assertEquals(dismissMessage.getText(),"You selected Cancel");
    }
    @Test//10
    public void frames(){
        List<WebElement> FrameCards = driver.findElements(By.cssSelector(".category-cards>div"));
        WebElement cardAlertsFrameWindows = FrameCards.get(2);
        js.executeScript("arguments[0].scrollIntoView();", cardAlertsFrameWindows);
        cardAlertsFrameWindows.click();
        List<WebElement> listItems = driver.findElements(By.cssSelector(".menu-list>#item-2"));
        WebElement frames = listItems.get(1);
        frames.click();
        WebElement outFrame=driver.findElement(By.cssSelector("#frame1"));
        driver.switchTo().frame(outFrame);
        WebElement samplePage= driver.findElement(By.cssSelector("#sampleHeading"));
      Assert.assertEquals(samplePage.getText(),"This is a sample page");
        driver.switchTo().defaultContent();
    }
    @Test//11
    public void selectable() {
        List<WebElement> FiveCards = driver.findElements(By.cssSelector(".category-cards>div"));
        WebElement cardInteractions = FiveCards.get(4);
        js.executeScript("arguments[0].scrollIntoView();", cardInteractions);
        cardInteractions.click();
        List<WebElement> listItems = driver.findElements(By.cssSelector(".menu-list>#item-1"));
        WebElement selectable = listItems.get(3);
        js.executeScript("arguments[0].scrollIntoView();", selectable);
        selectable.click();
        WebElement grid = driver.findElement(By.cssSelector("a[id=\"demo-tab-grid\"]"));
        grid.click();
        List<WebElement> gridListFive = driver.findElements(By.cssSelector("#row2>li"));
        WebElement five = gridListFive.get(1);
        five.click();
        wait.until(ExpectedConditions.attributeToBe(five, "class", "list-group-item active list-group-item-action"));
        Assert.assertEquals(five.getAttribute("class"), "list-group-item active list-group-item-action", "Five is not chosennn");

    }


    @Test//12
    public void dragAndDrop() throws InterruptedException {
        Actions action = new Actions(driver);
        List<WebElement> DroppedCards = driver.findElements(By.cssSelector(".category-cards>div"));
        WebElement cardInteractions = DroppedCards.get(4);
        js.executeScript("arguments[0].scrollIntoView();", cardInteractions);
        cardInteractions.click();
        List<WebElement> listItems = driver.findElements(By.cssSelector(".menu-list>#item-3"));
        WebElement droppable = listItems.get(3);
        droppable.click();
        WebElement  drag=driver.findElement(By.cssSelector("#draggable"));
        WebElement dropHere=driver.findElement(By.cssSelector("#droppable"));
        action.dragAndDrop(drag,dropHere).build().perform();
        Thread.sleep(3000);
        List<WebElement> text = driver.findElements(By.cssSelector("div[id=\"droppable\"]>p"));
        WebElement textDropped = text.get(0);
        Assert.assertEquals(textDropped.getText(), "Dropped!", "Drop here");




    }


    @AfterMethod
    public void tearDown() {

        driver.quit();
    }

}
